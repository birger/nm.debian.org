from django.conf import settings
import tempfile
import git
import os
import sys
import subprocess
import shlex
import datetime
import smtplib
import email.message
import logging
import logging.handlers

log = logging.getLogger("deploy")

class Fail(Exception):
    pass


class Deployer:
    def __init__(self, dry_run=False, root=None):
        self.dry_run = dry_run
        if root is None:
            self.repo = git.Repo(search_parent_directories=True)
        else:
            self.repo = git.Repo(root)
        self.branch = getattr(settings, "DEPLOY_BRANCH", "origin/master")
        self.queue_dir = getattr(settings, "DEPLOY_QUEUE_DIR", None)
        if self.queue_dir is None:
            raise Fail("DEPLOY_QUEUE_DIR setting has not been set")
        self.queued_shasums = set(x[:-7] for x in os.listdir(self.queue_dir) if x.endswith(".deploy"))
        if not self.queued_shasums:
            raise Fail("No .deploy files found in {}".format(self.queue_dir))

    def select_branch(self, fetch=True):
        if fetch:
            log.debug("Fetching from origin")
            self.repo.remotes.origin.fetch()
        self.ref = self.repo.references[self.branch]
        if self.ref.commit.hexsha not in self.queued_shasums:
            raise Fail("Branch {} has shasum {} but {} are queued for deploy".format(self.branch, self.ref.commit.hexsha, ", ".join(sorted(self.queued_shasums))))
        log.info("Selected %s (%s) %s", self.branch, self.ref.commit.hexsha, self.ref.commit.summary)
        dt = datetime.datetime.utcfromtimestamp(self.ref.commit.committed_date)
        log.info("Committed by %s <%s> on %s", self.ref.commit.author.name, self.ref.commit.author.email, dt.strftime("%Y-%m-%d %H:%M UTC"))

    def validate_commit(self):
        keys_dir = os.path.abspath(getattr(settings, "DEPLOY_KEYS_DIR", "deploy/keyring"))
        with tempfile.TemporaryDirectory(suffix=".keyring") as keyring_dir:
            log.debug("Creating gpg keyring")
            os.environ["GNUPGHOME"] = keyring_dir
            keyfiles = []
            for name in os.listdir(keys_dir):
                if not name.endswith(".gpg"): continue
                keyfiles.append(os.path.join(keys_dir, name))
                log.debug("Found key file %s", name)
            if not keyfiles:
                log.warn("No key files found in %s", keys_dir)
            cmd = subprocess.run(
                    ["gpg", "--no-autostart", "--import"] + keyfiles,
                    stdin=subprocess.DEVNULL,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
            )
            if cmd.returncode != 0:
                raise Fail("failed to import keys into the keyring")

            current_head = self.repo.head.commit.hexsha
            if not self.repo.is_ancestor(current_head, self.branch):
                raise Fail("current head {} is not an ancestor of {}".format(current_head, self.branch))
            log.info("current head %s is an ancestor of %s", current_head, self.branch)

            try:
                res = self.repo.git.verify_commit(self.ref)
            except git.exc.GitCommandError as e:
                raise Fail("commit to deploy is not signed or has an invalid signature")
            log.info("%s has a valid signature", self.branch)

    def run(self, cmd):
        cmd_formatted = " ".join(shlex.quote(x) for x in cmd)
        if self.dry_run:
            log.info("Would run %s in %s", cmd_formatted, self.repo.working_dir)
        else:
            log.info("Running %s in %s", cmd_formatted, self.repo.working_dir)
            res = subprocess.run(cmd, cwd=self.repo.working_dir, stdin=subprocess.DEVNULL, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            for line in res.stdout.splitlines():
                log.info("%s stdout: %s", cmd_formatted, line)
            for line in res.stderr.splitlines():
                log.warn("%s stderr: %s", cmd_formatted, line)
            if res.returncode != 0:
                raise Fail("%s exited with result %d", cmd_formatted, res.returncode)

    def deploy(self):
        self.run(["git", "rebase", self.branch])
        self.run(["./manage.py", "collectstatic", "--noinput"])
        self.run(["./manage.py", "migrate"])
        self.run(["psql", "service=nm", "-c",
                  "grant select,insert,update,delete on all tables in schema public to nmweb"])
        self.run(["psql", "service=nm", "-c",
                  "grant usage on all sequences in schema public to nmweb"])
        self.run(["touch", "nm2/wsgi.py"])

    def cleanup(self):
        for hexsha in self.queued_shasums:
            pathname = os.path.join(self.queue_dir, hexsha) + ".deploy"
            log.info("Removing %s", pathname)
            if not self.dry_run:
                os.unlink(pathname)
