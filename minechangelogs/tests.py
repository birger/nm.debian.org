from django.test import TestCase
from django.urls import reverse
from django.utils.timezone import now
from django.core import mail
from backend import const
from backend import models as bmodels
from backend.unittest import PersonFixtureMixin
from contextlib import contextmanager
import minechangelogs.models as mmodels
from unittest.mock import patch
import tempfile
import os


class TestMinechangelogs(PersonFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.url = reverse("minechangelogs_search", kwargs={ "key": cls.persons.dc.lookup_key })

    @classmethod
    def __add_extra_tests__(cls):
        cls._add_method(cls._test_forbidden, None)
        for visitor in "pending", "dc", "dc_ga", "dm", "dm_ga", "dd_e", "dd_r",  "dd_nu", "dd_u", "fd", "dam":
            cls._add_method(cls._test_success, visitor)

    @contextmanager
    def index(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            with self.settings(MINECHANGELOGS_INDEXDIR=os.path.join(tmpdir, "index"), MINECHANGELOGS_CACHEDIR=os.path.join(tmpdir, "cache")):
                indexer = mmodels.Indexer()
                indexer.index([
                    ("test_0.1", "Sun, 11 Dec 2016 08:15:50", "test@debian.org", "thanks test for #123"),
                    ("test_0.2", "Sun, 12 Dec 2016 08:15:50", "test@debian.org", "test tests for #124"),
                ])
                indexer.flush()
                yield

    def _test_success(self, visitor):
        with self.index():
            client = self.make_test_client(self.persons[visitor])
            response = client.get(self.url)
            self.assertEqual(response.status_code, 200)

            response = client.post(self.url, data={"query": "test"})
            self.assertEqual(response.status_code, 200)

            response = client.post(self.url, data={"query": "test", "download": True})
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response["Content-Type"], "text/plain")

    def _test_forbidden(self, visitor):
        with self.index():
            client = self.make_test_client(self.persons[visitor])
            response = client.get(self.url)
            self.assertPermissionDenied(response)
